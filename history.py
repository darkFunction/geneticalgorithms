import csv
import matplotlib.pyplot as plotter
import matplotlib.ticker as mticker
import matplotlib.dates as mdates
import numpy as np
import time
import datetime 

def graphData(fileName):
	try:
		date, time, openp, highp, lowp, closep, volume = np.loadtxt(fileName, 
																	delimiter=',', 
																 	unpack=True, 
																	converters={0: mdates.strpdate2num('%Y%m%d')})

		ax1 = plotter.subplot(1, 1, 1)

		ax1.xaxis.set_major_locator(mticker.MaxNLocator(10))
		ax1.xaxis_set_major_formatter(mdates.DateFormatter('%m-%d'))

		ax1.plot(date, openp)
		ax1.plot(date, highp)
		ax1.plot(date, lowp)
		ax1.plot(date, closep)

		plotter.show()

	except Exception, e:
		print "Failed main loop", str(e)

graphData('btce2014.csv')
