require 'pl'

math.randomseed os.time() 

class FunctionWrapper
	new: (@func, @childCount, @name) =>

class FunctionNode
	new: (@functionWrapper, @children) =>
	evaluate: (param) =>
		results = [child\evaluate(param) for child in *@children]
		return @functionWrapper.func(results)
	display: (indent=0) =>
		print string.rep('   ', indent)..@functionWrapper.name
		for child in *@children
			child\display(indent + 1)

class ParamNode
	new: (@index) =>
	evaluate: (params) =>
		return params[@index]
	display: (indent=0) =>
		print string.rep('   ', indent)..@index

class ConstNode
	new: (@value) =>
	evaluate: (parameter) =>
		return @value
	display: (indent=0) =>
		print string.rep('   ', indent)..@value

wrappers = {
	FunctionWrapper( ((a) -> return a[1] + a[2]), 2, "add" ),
	FunctionWrapper( ((a) -> return a[1] - a[2]), 2, "sub" ),
	FunctionWrapper( ((a) -> return a[1] * a[2]), 2, "mul" ),
	FunctionWrapper( ((a) -> if a[1] then return a[2] else return a[3]), 3, "if"),
	FunctionWrapper( ((a) -> if a[1] > a[2] then return 1 else return 0), 2, ">")		
}

makeRandomTree = (paramCount, maxDepth=4, probIsFunc=0.5, probIsParam=0.6) ->
	if math.random! < probIsFunc and maxDepth > 0
		funcWrapper = wrappers[math.random(#wrappers)]
		children = [ makeRandomTree(paramCount, maxDepth-1, probIsFunc, probIsParam) for i = 1, funcWrapper.childCount ]
		return FunctionNode(funcWrapper, children)
	if math.random! < probIsParam
		return ParamNode(math.random(paramCount))
	return ConstNode(math.random(10))

hiddenFunction = (x, y) -> 
	return x*x+2*y+3*x+5

buildHiddenSet = () ->
	rows = {}
	for i=1, 200
		x = math.random(40)
		y = math.random(40)
		rows[#rows+1] = {x, y, hiddenFunction(x, y)}
	return rows

scoreFunction = (funcTree, dataSet) ->
	diff = 0
	for data in *dataSet
		value = funcTree\evaluate {data[1], data[2]}
		diff += math.abs (value - data[3])
	return diff

mutate = (funcTree, paramCount, probChange=0.1) ->
	if math.random() < probChange
		return makeRandomTree paramCount
	result = tablex.deepcopy funcTree
	if funcTree.__class.__name == "FunctionNode"
		result.children = [mutate(child, paramCount, probChange) for child in *funcTree.children]
	return result

crossover = (tree1, tree2, probSwap=0.7, top=1) ->
	if math.random() < probSwap and not top
		return tablex.deepcopy tree2
	result = tablex.deepcopy tree1
	if tree1.__class.__name == "FunctionNode" and tree2.__class.__name == "FunctionNode"
		result.children = [crossover(child, tree2.children[math.random(#tree2.children)], probSwap, 0) for child in *tree1.children]
	return result

evolve = (paramCount, popSize, rankFunc, maxGen=500, mutationRate=0.1, breedingRate=0.4, probElitism=0.7, probNew=0.05) ->
	-- Random number tending toward lower numbers- the lowe probElitism is, the lower the numbers
	selectIndex = () ->
		return math.ceil( math.log(math.random!) / math.log(probElitism) ) 

	-- Create a random initial population
	population = [makeRandomTree(paramCount) for i=1, popSize]
	scores = nil
	for i=1, maxGen
		scores = rankFunc population
		print scores[1][1]
		if scores[1][1] == 0
			break

		-- The two best always make it
		newPopulation = {scores[1][2], scores[2][2]}
		
		-- Build next generation
		while #newPopulation < popSize
			if math.random! < probNew
				-- New random node introduced
				newPopulation[#newPopulation+1] = makeRandomTree(paramCount)
			else
				lucky1 = scores[selectIndex!][2]
				lucky2 = scores[selectIndex!][2]
				cross = crossover(lucky1, lucky2, breedingRate)	
				child = mutate(cross, paramCount, mutationRate)
				newPopulation[#newPopulation+1] = child

		population = newPopulation

	scores[1][2]\display!
	return scores[1][2]

getRankFunction = (dataSet) ->
	rankFunc = (population) ->
		scores = [{scoreFunction(tree, dataSet), tree} for tree in *population]
		sortFunc = (ax, bx) ->
			return scores[ax][1] < scores[bx][1]
		sorted = {}
		for k,v in tablex.sort(scores, sortFunc) 
			sorted[#sorted + 1] = v 
		return sorted 
	return rankFunc

-- Let's go!
evolve 2, 500, getRankFunction(buildHiddenSet!), 500, 0.2, 0.1, 0.7, 0.1

