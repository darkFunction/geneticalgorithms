import random
import math
import numpy
from PIL import Image, ImageDraw, ImageTk
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from Tkinter import Tk, Canvas

# shapes are genes, have 50
#class Shape:
#	def __init__(verts, col, pos):
#		self.verts = verts 
#		self.col = col
#		self.pos = pos
#	def mutate():
		



## read image as RGB and add alpha (transparency)
#im = Image.open("image.jpg").convert("RGBA")
#
## convert to numpy (for convenience)
#imArray = numpy.asarray(im)
#
## create mask
#polygon = [(444,203),(623,243),(691,177),(581,26),(482,42)]
#maskIm = Image.new('L', (imArray.shape[1], imArray.shape[0]), 0)
#ImageDraw.Draw(maskIm).polygon(polygon, outline=1, fill=1)
#mask = numpy.array(maskIm)
#
## assemble new image (uint8: 0-255)
#newImArray = numpy.empty(imArray.shape,dtype='uint8')
#
## colors (three first columns, RGB)
#newImArray[:,:,:3] = imArray[:,:,:3]
#
## transparency (4th column)
#newImArray[:,:,3] = mask*255
#
## back to Image from numpy
##newIm = Image.fromarray(newImArray, "RGBA")
##newIm.save("out.png")
##newIm.show()
#

def randPoly(vertCount, maxX, maxY, minSize, maxSize):
	angles = []
	for i in range(vertCount):
		if (i > vertCount / 2):
			angles.append(random.uniform(math.pi, math.pi*2))
		else:
			angles.append(random.uniform(0, math.pi))
	angles.sort()
	polygon = []
	offset = maxSize / 2
	for i in range(vertCount):
		radius = random.randint(minSize, maxSize/2)
		x = offset + radius * math.cos(angles[i])
		y =	offset + radius * math.sin(angles[i])
		polygon.append((x, y))
	return polygon

def randColour():
	return (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

width, height = 512, 512
img = Image.new('RGBA', (width, height), (0, 0, 0, 255))
for i in range(50):
	polygon = randPoly(random.randint(3, 6), width, height, 20, 256)  
	polyImage = Image.new('RGBA', (256, 256))
	ImageDraw.Draw(polyImage).polygon(polygon, fill=randColour())
	img.paste(polyImage, (128, 128), mask=polyImage)
img.show()
root = Tk()
photoImage = ImageTk.PhotoImage(img)
root.geometry('512x512')
canvas = Canvas(root, width=512, height=512)
canvas.create_image(256, 256, image=photoImage)
canvas.pack()
root.mainloop()

#img_size = (512,512)
#poly_size = (256,256)
#poly_offset = (128,128) #location in larger image
#back = Image.new('RGBA', img_size, (255,0,0,0) )
#poly = Image.new('RGBA', poly_size )
#pdraw = ImageDraw.Draw(poly)
#pdraw.polygon([ (0,0), (256,256), (0,256), (256,0)], 
#			               fill=(255,255,255,127), outline=(255,255,255,255))
#back.paste(poly, poly_offset, mask=poly)
#back.show()


#imgArray = numpy.array(img)
#imgArray = numpy.empty([256, 256, 4], dtype='uint8')
#imgArray[:,:] = 255, 255, 255, 255
#plt.imshow(imgArray)
#plt.show()
