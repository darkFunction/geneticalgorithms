from random import random, randint, choice
from copy import deepcopy
from math import log

class FunctionWrapper:
	def __init__(self, function, childCount, name):
		self.function = function
		self.childCount = childCount
		self.name = name

class FunctionNode:
	def __init__(self, functionWrapper, children):
		self.function = functionWrapper.function
		self.name = functionWrapper.name
		self.children = children

	def evaluate(self, parameter):
		results = [child.evaluate(parameter) for child in self.children]
		return self.function(results)
	
	def display(self, indent=0):
		print('    '*indent) + self.name
		for child in self.children:
			child.display(indent+1)

class ParameterNode:
	def __init__(self, index):
		self.index = index

	def evaluate(self, parameter):
		return parameter[self.index]

	def display(self, indent=0):
		print '%sp%d' % ('    '*indent, self.index)

class ConstantNode:
	def __init__(self, value):
		self.value = value

	def evaluate(self, parameter):
		return self.value
	
	def display(self, indent=0):
		print '%s%d' % ('    '*indent, self.value)

addWrapper = FunctionWrapper(lambda a:a[0]+a[1], 2, "add")
subWrapper = FunctionWrapper(lambda a:a[0]-a[1], 2, "sub")
mulWrapper = FunctionWrapper(lambda a:a[0]*a[1], 2, "mul")
def ifFunc(a):
	if a[0]:
		return a[1]
	return a[2]
ifWrapper = FunctionWrapper(ifFunc, 3, "if")
def isGreater(a):
	if a[0]>a[1]:
		return 1
	return 0
isGreaterWrapper = FunctionWrapper(isGreater, 2, 'isGreater')

functionList = [addWrapper, subWrapper, mulWrapper, ifWrapper, isGreaterWrapper]

def makeRandomTree(paramCount, maxDepth=4, probIsFunc=0.5, probIsParam=0.6):
	if random() < probIsFunc and maxDepth > 0:
		funcWrapper = choice(functionList)
		children = [makeRandomTree(paramCount, maxDepth-1, probIsFunc, probIsParam) for i in range(funcWrapper.childCount)]
		return FunctionNode(funcWrapper, children)
	if random() < probIsParam:
		return ParameterNode(randint(0, paramCount-1))
	return ConstantNode(randint(0,10))

def hiddenFunction(x, y):
	return x**2+2*y+3*x+5

def buildHiddenSet():
	rows = []
	for i in range(200):
		x=randint(0, 40)
		y=randint(0, 40)
		rows.append([x, y, hiddenFunction(x,y)])
	return rows

def scoreFunction(functionTree, aSet):
	diff = 0
	for data in aSet:
		value = functionTree.evaluate([data[0], data[1]])
		diff += abs(value-data[2])
	return diff

def mutate(functionTree, paramCount, probChange=0.1):
	if random() < probChange:
		return makeRandomTree(paramCount)	
	result = deepcopy(functionTree)
	if isinstance(functionTree, FunctionNode):
		result.children = [mutate(child, paramCount, probChange) for child in functionTree.children]
	return result

def crossover(tree1, tree2, probSwap=0.7, top=1):
	if random() < probSwap and not top:
		return deepcopy(tree2)
	result = deepcopy(tree1)
	if isinstance(tree1, FunctionNode) and isinstance(tree2, FunctionNode):
		result.children = [crossover(child, choice(tree2.children), probSwap, 0) for child in tree1.children]
	return result

def evolve(paramCount, populationSize, rankFunction, maxGenerations=500, mutationRate=0.1, breedingRate=0.4, probElitism=0.7, probNew=0.05):
	# Random number tending towards lower numbers- the lower probElitism is, the lower the numbers
	def selectIndex():
		return int(log(random())/log(probElitism))
	
	# Create a random initial population
	population = [makeRandomTree(paramCount) for i in range(populationSize)]
	for i in range(maxGenerations):
		scores = rankFunction(population)
		print scores[0][0]
		if scores[0][0] == 0:
			break;

		# The two best always make it
		newPopulation = [scores[0][1], scores[1][1]]

		# Build next generation
		while len(newPopulation) < populationSize:
			if random() < probNew:
				# Add a new random node to mix things up
				newPopulation.append(makeRandomTree(paramCount))
			else:
				tree = mutate(crossover(scores[selectIndex()][1], scores[selectIndex()][1], probSwap=breedingRate),
							  paramCount, 
							  probChange = mutationRate)	
				newPopulation.append(tree)

		population = newPopulation
	scores[0][1].display()
	return scores[0][1]

def getRankFunction(dataset):
	def rankFunction(population):
		scores = [(scoreFunction(tree, dataset), tree) for tree in population]
		scores.sort()
		return scores
	return rankFunction
	

def testRandomMutations(bestScore):
	hiddenSet = buildHiddenSet()
	functionTree = makeRandomTree(2, 5)
	for i in range(0, 15000):
		score = scoreFunction(functionTree, hiddenSet)
		if score < bestScore:
			bestScore = score
			functionTree.display()
			print('>> ' + str(score))
		functionTree = mutate(functionTree, 2)
	test(bestScore)

evolve(2, 500, getRankFunction(buildHiddenSet()), mutationRate=0.2, breedingRate=0.1, probElitism=0.7, probNew=0.1)
